<p align="center">
<img src="https://forum.kaspersky.com/uploads/monthly_2022_11/image.png.e87018ba12b8c6998973f4d2f237f1e2.png"/>
</p>

<h1>Windows 10 Virtual Machine - Prerequisites and Installation</h1>
This guide shows how to make a Windows 10 Virtual Machine in VirtualBox!<br />

<h2>Environments and Technologies Used</h2>

- Firefox Browser
- User-Agent Switcher and Manager (Browser Extension)
- Oracle VM VirtualBox

<h2>Operating Systems Used </h2>

- Windows 10</b> (22H2)

<h2>List of Prerequisites</h2>

- Check if you have virtualization or SVM mode enabled on in your BIOS settings. (known as Intel VMX and AMD Hyper-V)
- Any Internet Browser of your choice (preferably use Firefox if possible)
- Oracle VM VirtualBox

<h2>Installation Steps</h2>

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/IBO83X5YlnwZ/dY6mY6oOHS8GQsu4qk2Mbch7jZEcnTkC7ThM7v5d.png" height="50%" width="50%"/>
</p>
<p>
Start by adding the "User-Agent Switcher and Manager" browser extension. You can try to use any other browser but it is most compatible with Firefox.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/ADt5Fsmy3Nn5/chSxa4UIVHQFFQMi6vrnLlIWbDZEKH3UHnWs6oMd.png" height="50%" width="50%"/>
</p>
<p>
Search up "download windows 10 iso file" and click the first link you see or copy paste this link into your address bar.

https://www.microsoft.com/en-us/software-download/windows10

Click on the "User-Agent Switcher and Manager" on the top right. If it is not there click the puzzle piece icon and right click the extension to pin it to the toolbar. Make sure the details look same as number 1 and 2 in the screenshot. After that, click "Apply (container on window)" then click "Refresh Tab" shown as number 3 and number 4 in the screenshot.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/bVNCxLEV3CrB/K0WOqlhcLI6BjsVwqe1CVtlrNfDuaMyAnJc80PRE.png" height="50%" width="50%"/>
</p>
<p>
Now the website should appear to look like this or have something similar to this.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/957aYxWx6PVp/CuE8wIdKQt72WF2PgT7QvcexxCPxyKyifhnRMt0X.png" height="50%" width="50%"/>
</p>
<p>
Select "Windows 10 (multi-edition ISO)" and any language of preference then confirm.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/PmioWjb6NAjW/LH0JulJZXWMmzvlWf4NLjyAdyRCvYB06tNSZW4nC.png" height="50%" width="50%"/>
</p>
<p>
Select "64-bit download" to start the download of the ISO file.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/iCrY2HVp4Eu5/Qug76LCVo9bYuvJmmp3Dxr3nzOvUOVtO4kJ3SqJk.png" height="50%" width="50%"/>
</p>
<p>
Remember where this file is stored. It is going to be used to make the virtual machine in VirtualBox.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/iFtmy9hhoPKq/86GZggLG15EhJZb1R1GpfhQsgfaO5SyClZCCQnOk.png" height="25%" width="25%"/>
</p>
<p>
Feel free to remove the extension after you are done downloading the ISO file.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/9mW2qDhzlUhO/3CqyWhVGbrPMaB8MqtXVDO23Awanea1n9tbiKnda.png" height="50%" width="50%"/>
</p>
<p>
Open VirtualBox, then click "New".
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/Ozy0GxpMSAzK/wxVE1vzsEngVb8l1pxZwu2Nvu6Kbw3U820D2rYbP.png" height="50%" width="50%"/>
</p>
<p>
1. Give your virtual machine a name.

2. Pick a location of where the virtual machine is going to be stored.

3. Select the ISO file that you have downloaded.

4. Click the "Skip Unattended Installation". The installation process of Windows will be bugged if this is left unchecked.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/Gy2Sjm3jgS1p/zGzjrYCK7VJuw91XNB5elR90orhYzLj0SlKC3Ssh.png" height="50%" width="50%"/>
</p>
<p>
1. It's recommended to allocate 4098 MBs (or 4 GBs) of RAM to use windows.

2. At least have 2 virtual cores to run this virtual machine. Remember that you are sharing resources with this virtual machine. Sharing too many resources may cause your PC to freeze and you may have to restart your computer to change its settings.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/BNiXZOikIRJr/ipKNDLq97DDhTm0Sb3GdV1B9HwQEsE6cblPS9ifj.png" height="50%" width="50%"/>
</p>
<p>
Use whatever size you want for the virtual hard disk. Leave "Pre-allocate Full Size" unchecked.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/gGYJA460p3hD/ghGkEYQLsIiD8Fl9NhGS1R2ml2ReZMOZtcQu4idu.png" height="50%" width="50%"/>
</p>
<p>
Once everything looks good, click "Finish".
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/XYmSK1TSBsYe/dcMM4Mu3mxE39FASroLmhGktEzoGXR3Ja7FxJcPn.png" height="50%" width="50%"/>
</p>
<p>
Now the virtual machine will appear in your main manager. To run the VM, simply double click it.
</p>
<br />

<p>
<img src="https://pxscdn.com/public/m/_v2/678732172940968681/c6a394f69-21cf85/y3y1Ee8WruYl/S7hAfsRIXlLUrU0UbN0dLlIEvDovD8TMNs25uY4P.png" height="50%" width="50%"/>
</p>
<p>
If this appears on your screen, then you have successfully made a Windows 10 Virtual Machine. You can start setting it up like it is another computer!

Enjoy!
</p>
<br />

